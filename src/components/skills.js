import React from 'react';
import './skills.css';
import reactjslogo from '../images/reactjs.png';
import html5logo from '../images/html5.png';
import css3logo from '../images/css3.svg';
import phplogo from '../images/php.svg';
import wordpresslogo from '../images/wordpress.png';
import jquerylogo from '../images/jquery.png';
import laravellogo from '../images/laravel.svg';
import bootstraplogo from '../images/bootstrap.svg';
import mysqllogo from '../images/mysql.svg';


class Skills extends React.Component{
    render(){
        return(
            <div className="skills-wrapper">
                <div className="container">
                    <div className="information">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="general-info">
                                    <h2 className="skills-title">
                                        General Info
                                    </h2>
                                    <div className="basic-info">
                                        <ul>
                                            <li><span>Date of Birth</span> Nov 7, 1996</li>
                                            <li><span>Address</span> Nayathimi, Bhaktapur</li>
                                            <li><span>E-mail</span> nirajanrijal53@gmail.com</li>
                                            <li><span>Phone</span> +977 9816257665</li>
                                        </ul>
                                    </div>
                                    <div className="social-icon">
                                        <ul>
                                            <li><a href="https://www.facebook.com/nirajanrijal53" target="_blank" rel="noopener noreferrer"><i className="fa fa-facebook"></i></a></li>
                                            <li><a href="https://www.instagram.com/nirajanrijal123/" target="_blank" rel="noopener noreferrer"><i className="fa fa-instagram"></i></a></li>
                                            <li><a href="https://twitter.com/NirajanRijal6" target="_blank" rel="noopener noreferrer"><i className="fa fa-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <h2 className="skills-title">
                                    Expertise
                                </h2>
                                <div className="expertise-images">
                                    <img src={ html5logo } alt="html5 logo"/>
                                    <img src={ bootstraplogo } alt="bootstrap logo"/>
                                    <img src={ css3logo } alt="css3 logo"/>
                                    <img src={ wordpresslogo } alt="wordpress logo"/>
                                    <img src={ reactjslogo } className="reactlogo" alt="reatjs logo"/>
                                    <img src={ laravellogo } alt="laravel logo"/>
                                    <img src={ mysqllogo } alt="mysql logo"/>
                                    <img src={ jquerylogo } alt="jquery logo"/>
                                    <img src={ phplogo } alt="php logo"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Skills;