import React from 'react';
import './education.css';

class Education extends React.Component{
    render(){
        return(
            <div className="education-wrapper" id="education">
                <div className="container">
                    <div className="education-content">
                        <h2 className="education-title">
                            Education
                        </h2>
                        <div className="education-single">
                            <h2 className="education-time">2013-2017</h2>
                            <h3 className="education-board">B.E. in Computer Engineering </h3>
                            <h4 className="education-institute">Tribhuvan University, Institute of Engineering (IOE), Purwanchal Campus, Dharan. </h4>
                        </div>

                        <div className="education-single">
                            <h2 className="education-time">2011-2013</h2>
                            <h3 className="education-board">Intermediate level </h3>
                            <h4 className="education-institute">Little Angels' H S School, Hattiban, Lalitpur. </h4>
                        </div>

                        <div className="education-single">
                            <h2 className="education-time">2011</h2>
                            <h3 className="education-board">SLC  </h3>
                            <h4 className="education-institute">Hetauda Academy, Hetauda, Makawanpur. </h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Education;