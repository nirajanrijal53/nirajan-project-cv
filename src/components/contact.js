import React from 'react';
import './contact.css';

class Contact extends React.Component{
    render(){
        return(
            <div className="contact-wrapper" id="contact">
                <div className="container">
                    <div className="contact-content">
                        <h2 className="contact-title">
                            Contact
                        </h2>
                        <form action="#">
                            <label htmlFor="name">Full Name</label>
                            <input type="text" id="name" name="fullname" placeholder="Full Name"/>

                            <label htmlFor="email">Email</label>
                            <input type="email" id="email" name="email" placeholder="Email"/>

                            <label htmlFor="phone">Phone Number</label>
                            <input type="number" id="phone" name="phone" placeholder="Phone Number"/>

                            <label htmlFor="message">Message</label>
                            <textarea type="text" id="message" name="message" placeholder="Message"/>

                            <input type="submit" value="Submit"/>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Contact;