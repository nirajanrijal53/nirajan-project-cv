import React from 'react';
import './header.css';

class Header extends React.Component {
    render(){
        return(
            <header>
                <nav>
                    <div className="container">
                        <div className="navbar">
                            <div className="nav-logo">
                                <a href="/"> n </a>
                            </div>
                            <div className="nav-list">
                                <ul>
                                    <li><a href="#home">Home</a></li>
                                    <li><a href="#experience">Experience</a></li>
                                    <li><a href="#portfolio">Portfolio</a></li>
                                    <li><a href="#education">Education</a></li>
                                    <li><a href="#contact">Contact</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>
        );
    }
}

export default Header;