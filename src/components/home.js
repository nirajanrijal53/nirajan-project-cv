import React from 'react';
import './home.css';

class Home extends React.Component{
    render(){
        return(
            <div className="banner-wrapper" id="home">
                <div className="banner-content">
                    <h1 className="banner-title">Nirajan Rijal</h1>
                    <p className="banner-subtitle">Frontend &amp; WordPress Developer</p>
                </div>
            </div>
        );
    }
}

export default Home;