import React from 'react';
import './footer.css';

class Footer extends React.Component{
    render(){
        let date = new Date();
        return(
            <footer>
                <div className='site-info'>
                    Copyright &copy; { date.getFullYear() } All rights reserved | Designed and Developed by Nirajan Rijal.
                </div>
            </footer>
        );
    }
}

export default Footer;