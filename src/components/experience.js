import React from 'react';
import './experience.css';

class Experience extends React.Component{
    render(){
        return(
            <div className="exp-wrapper" id="experience">
                <div className="container">
                    <div className="exp-content">
                        <h2 className="exp-title">
                            Work Experience
                        </h2>
                        <div className="exp-single">
                            <h2 className="exp-time">Dec, 2018-Present</h2>
                            <h3 className="exp-position">- Frontend &amp; WordPress Developer </h3>
                            <h4 className="exp-company">Nepware Pvt Ltd</h4>
                            <p className="exp-desc">Starting as a backend developer, learned new tools and technologies and worked as a Junior software engineer.</p>
                        </div>

                        <div className="exp-single">
                            <h2 className="exp-time">Feb, 2017 – June, 2018</h2>
                            <h3 className="exp-position">- Frontend Web Developer</h3>
                            <h4 className="exp-company">Ideabreed IT solution</h4>
                            <p className="exp-desc">Worked as an intern. Did frontend designing for websites. Developed communication skills with the team members. </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Experience;