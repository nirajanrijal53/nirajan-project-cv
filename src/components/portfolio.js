import React from 'react';
import './portfolio.css';

class Portfolio extends React.Component{
    render(){
        return(
            <div className="portfolio-wrapper" id="portfolio">
                <div className="container">
                    <div className="portfolio-content">
                        <h2 className="portfolio-title">
                            Portfolio
                        </h2>
                        <div className="portfolios">
                            <div className="row">
                                <div className="col-md-4">
                                    <a href="http://skinartsnepal.com/" target="_blank" rel="noopener noreferrer">
                                        <div className="portfolio-single port1">
                                            <div className="site">
                                                <h3>SkinArts</h3>
                                                <h4>Visit Website</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div className="col-md-4">
                                    <a href="http://dhi.nepware.com/" target="_blank" rel="noopener noreferrer">
                                        <div className="portfolio-single port2">
                                            <div className="site">
                                                <h3>DHI Nepal</h3>
                                                <h4>Visit Website</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div className="col-md-4">
                                    <a href="http://exponent.com.np/" target="_blank" rel="noopener noreferrer">
                                        <div className="portfolio-single port3">
                                            <div className="site">
                                                <h3>Exponent</h3>
                                                <h4>Visit Website</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div className="col-md-4">
                                    <a href="#home" target="_blank" rel="noopener noreferrer">
                                        <div className="portfolio-single port4">
                                            <div className="site">
                                                <h3>Asian Dental</h3>
                                                <h4>Visit Website</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                                <div className="col-md-4">
                                    <a href="#home" target="_blank" rel="noopener noreferrer">
                                        <div className="portfolio-single port5">
                                            <div className="site">
                                                <h3>Machinery</h3>
                                                <h4>Visit Website</h4>
                                            </div>
                                        </div>
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Portfolio;