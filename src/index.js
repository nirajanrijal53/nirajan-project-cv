import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './components/header';
import Footer from './components/footer';
import Home from './components/home';
import Experience from './components/experience';
import Portfolio from './components/portfolio';
import Education from './components/education';
import Contact from './components/contact';
import Skills from './components/skills';

class App extends React.Component{
    render(){
        return(
            <div>
                <Header/>
                <Home/>
                <Skills/>
                <Experience/>
                <Portfolio/>
                <Education/>
                <Contact/>
                <Footer/>
            </div>
        );
    }
}

ReactDOM.render(<App/>, document.querySelector("#root"));